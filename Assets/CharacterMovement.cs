using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour

{
    // Start is called before the first frame update
    public Rigidbody rb;
    public float movespeed = 10f;

    private float xinput;
    private float zinput;

    void Start()
    {

    }

    void Update()
    {
        ProcessInputs();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void ProcessInputs()
    {
        xinput = Input.GetAxis("Horizontal");
        zinput = Input.GetAxis("Vertical");
    }

    private void Move()
    {
        rb.AddForce(new Vector3(xinput, 0f, zinput) * movespeed);
    }
}
